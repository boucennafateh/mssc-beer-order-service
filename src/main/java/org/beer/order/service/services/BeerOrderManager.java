package org.beer.order.service.services;

import org.beer.order.service.domain.BeerOrder;

import java.util.UUID;

public  interface BeerOrderManager {

    BeerOrder newBeerOrder(BeerOrder beerOrder);
    void processValidationResult(UUID beerOrderId, boolean isValid);
}
